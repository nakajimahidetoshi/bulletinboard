package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import beans.Message;
import beans.User;
import exception.SQLRuntimeException;

public class MessageDao {

    public void insert(Connection connection, Message message) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO messages ( ");
            sql.append("subject");
            sql.append(", text");
            sql.append(", category");
            sql.append(", user_id");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");

            sql.append(" ?"); // subject
            sql.append(", ?"); // text
            sql.append(", ?"); // category
            sql.append(", ?"); // user_id
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());
            ps.setString(1, message.getSubject());
            ps.setString(2, message.getText());
            ps.setString(3, message.getCategory());
            ps.setInt(4, message.getUserId());


            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public void delete(Connection connection, int id) {

    	PreparedStatement ps = null;
    	try {
    		String sql = "DELETE FROM messages WHERE id = ?";

    		ps = connection.prepareStatement(sql);
    		ps.setInt(1, id);

    		int cnt = ps.executeUpdate();

    	} catch (SQLException e) {
    		throw new SQLRuntimeException(e);
    	} finally {
    		close(ps);
    	}
    }

	public List<User> getUsers(Connection connection) {
    	PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE * from messages ");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<User> ret = toUserList(rs);
            return  ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
	private List<User> toUserList(ResultSet rs) {
		// TODO 自動生成されたメソッド・スタブ
		return null;
	}
	public List<Message> getdelete(Connection connection) {
		// TODO 自動生成されたメソッド・スタブ
		return null;
	}
	public void delete(Connection connection, Integer deleteId) {
		// TODO 自動生成されたメソッド・スタブ

	}




}