package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.UserService;

@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        int id = Integer.parseInt(request.getParameter("id"));

        User editUser = new UserService().getUser(id);
        request.setAttribute("editUser", editUser);

        request.getRequestDispatcher("settings.jsp").forward(request, response);
    }

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		User editUser = getEditUser(request);

		if (isValid(request, messages) == true) {

			try {
				new UserService().update(editUser);
			} catch (NoRowsUpdatedRuntimeException e) {
				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
				session.setAttribute("errorMessages", messages);
				request.setAttribute("editUser", editUser);
				request.getRequestDispatcher("settings.jsp").forward(request, response);
				return;
			}

			session.setAttribute("id", editUser);

			response.sendRedirect("management");
		} else {
			session.setAttribute("errorMessages", messages);
			request.setAttribute("editUser", editUser);
			request.getRequestDispatcher("settings.jsp").forward(request, response);
		}
	}
	private boolean isValid(HttpServletRequest request, List<String> messages) {
    	String name = request.getParameter("name");
    	String login_id = request.getParameter("login_id");
        String password = request.getParameter("password");
        String branch_id = request.getParameter("branch_id");
        String department_id = request.getParameter("department_id");
        if (StringUtils.isEmpty(name) == true) {
            messages.add("名前を入力してください");
        }
        if (StringUtils.isEmpty(login_id) == true) {
            messages.add("ログインIDを入力してください");
        }
        if (StringUtils.isEmpty(password) == true) {
            messages.add("パスワードを入力してください");
        }
        if (StringUtils.isEmpty(branch_id) == true) {
            messages.add("支店を入力してください");
        }
        if (StringUtils.isEmpty(department_id) == true) {
            messages.add("部署・役職を入力してください");
        }
        // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
	}
	private User getEditUser(HttpServletRequest request)
			throws IOException, ServletException {

		User editUser = new User();
		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setName(request.getParameter("name"));
		editUser.setAccount(request.getParameter("login_id"));
		editUser.setPassword(request.getParameter("password"));

		List<String> messages = new ArrayList<String>();

		if (isValid(request, messages) == true) {
			editUser.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
			editUser.setDepartment_id(Integer.parseInt(request.getParameter("department_id")));
		}
		return editUser;
	}

}