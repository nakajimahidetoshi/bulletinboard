<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>掲示板システム</title>

	</head>
	<body>
		<div class="main-contents">
			<div class="header">
				<c:if test="${ empty loginUser }">
					<a href="login">ログイン</a>
					<a href="signup">登録する</a>
				</c:if>
				<c:if test="${ not empty loginUser }">
					<a href="./">ホーム</a>
					<a href="management">ユーザー管理</a>
					<a href="newmessage">新規投稿</a>
					<a href="logout">ログアウト</a>
				</c:if>
			</div>
			<br>
			<div class="messages">
			    <c:forEach items="${messages}" var="message">
			            <div class="message">
							<div class="name">
		                    <span class="name">名前：<c:out value="${message.name}" /></span>
		                	</div>
							<div class="subject">件名：<c:out value="${message.subject}" /></div>
							<div class="category">カテゴリー：<c:out value="${message.category}" /></div><br>
			                <div class="text">本文：<c:out value="${message.text}" /></div>
			                <div class="date"><fmt:formatDate value="${message.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
			                <form action="deletemessage" method="post">
			                <input type="hidden" name="id" value="${message.id}">
				                <label for="id"></label><input type="submit" value="削除" /> <br />
				            </form>
			            </div>
			            <br>
			    </c:forEach>
			</div>
			<div class="copylight"> Copyright(c)Nakajima Hidetoshi</div>
		</div>
	</body>
</html>